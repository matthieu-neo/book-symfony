<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
    #[Route('/book', name: 'app_book', methods:['GET'])]
    public function index(): Response
    {
        return $this->render('book/index.html.twig', [
            'controller_name' => 'BookController',
        ]);
    }
    /**
     * @Route("/books", name="book_list", methods={"GET"})
     */
    public function list(): Response
    {
        return $this->render('book/index.html.twig');
    }

    /**
     * @Route("/books/add", name="book_add", methods={"GET", "POST"})
     */
    public function add(): Response
    {
        return $this->render('book/add_book.html.twig');
    }

    #[Route('/books/{id}', name: 'single_book', methods: ['GET', 'DELETE', 'PUT'], requirements: ['id' => '\d+'])]
   public function singleBook(): Response
   {
       return $this->render('book/single_book.html.twig');
   }

}
